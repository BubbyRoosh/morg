const Builder = @import("std").build.Builder;

pub fn build(b: *Builder) void {
    const target = b.standardTargetOptions(.{});
    const optimize = b.standardOptimizeOption(.{});
    const exe = b.addExecutable(.{
        .name = "morg",
        .root_source_file = .{ .path = "src/morg.c" },
        .target = target,
        .optimize = optimize,
    });

    exe.addIncludePath(.{ .path = "lib/" });
    exe.linkLibC();
    exe.linkSystemLibrary("tag_c");
    b.installArtifact(exe);
}
