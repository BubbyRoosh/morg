#ifndef __CONFIG_H
#define __CONFIG_H

#define PATH_MAX 2048

char *dirformat  = "$artist$ - $album$";
char *fileformat = "$track$ - $title$";
char *outpath    = "$HOME/Music";

#endif
