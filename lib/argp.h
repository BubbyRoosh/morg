/*
 * Copyright (c) 2021 BubbyRoosh <bubbyroosh@bubby.me>
 * 
 * Permission to use, copy, modify, and distribute this software for any
 * purpose with or without fee is hereby granted, provided that the above
 * copyright notice and this permission notice appear in all copies.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
 * WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
 * ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
 * WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
 * ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
 * OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
*/

#ifndef ARGP_H__
#define ARGP_H__
/* The contents of argv[0] when ARGBEGIN is called */
char *progname;

/* Starts iterating over each argument passed to the program, running each
 * character through a user-defined switch statement if the argument starts
 * with '-'
 */
#define ARGBEGIN for(progname=*argv++,argc--;*argv&&**argv=='-';argv++,argc--){\
                    int used_next = 0;\
                    while (*++*argv)\
                        switch(**argv)

/* Concludes the ARGBEGIN macro, skipping an argument if ARGNEXT is used. */
#define ARGEND     if (used_next && argc > 1) {\
                       argv++;\
                       argc--;\
                   }\
               } /* closes for loop from ARGBEGIN */

/* Gets the next argument and sets it to <str>. If there are no more arguments,
 * the <h> expression is ran and <str> is set to NULL
 */
#define ARGNEXT(h) (used_next++,argc>1?*(argv+1):((h),NULL))

#endif /* ARGP_H__ */
