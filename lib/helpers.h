#ifndef HELPERS_H__
#define HELPERS_H__
/* Replaces <search> with <replace> in <src>.
 * Returns: A **newly allocated** string with the replacements, or NULL on
 * alloc failure.
 */
char *
strrep(const char *src, const char *search, const char *replace)
{
    char *out, *nout;
    out = nout = NULL;
    size_t outlen = 0;
    size_t slen = strlen(search);
    size_t rlen = strlen(replace);
    for (;;) {
        char *next = strstr(src, search);
        if (!next) break;
        size_t srctofound = next - src;

        char *nout = realloc(out, (outlen + srctofound + rlen)*sizeof(char));
        if (!nout) goto defer;
        out = nout;

        memmove(out+outlen, src, srctofound);
        memmove(out+outlen+srctofound, replace, rlen);
        outlen += srctofound + rlen;
        src += srctofound + slen;
    }
    size_t srclen = strlen(src);

    nout = realloc(out, (outlen + srclen + 1)*sizeof(char));
    if (!nout) goto defer;
    out = nout;

    memmove(out + outlen, src, srclen);
    out[outlen + srclen] = '\0';
    return out;
defer:
    perror("realloc");
    if (out) free(out);
    return NULL;
}

int
mkdirall(char *pth)
{
    char *pthcpy = pth;
    for (;;) {
        char *next = strchr(pth, '/');
        if (!next) break;
        if (next == pthcpy) {
            pth++;
            continue;
        }
        size_t nlen = next-pth;
        pth[nlen] = 0;
        if (mkdir(pthcpy, 0700) == -1 && errno != EEXIST)
            return -1;
        pth[nlen] = '/';
        pth += nlen+1;
    }
    return 0;
}

#endif
