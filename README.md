# morg

![morg](./morg.png)

Tool for organizing music by the audio files' metadata.

## Installation

1. Ensure [taglib](https://taglib.org/) is installed.
2. Clone this repository: `git clone https://codeberg.org/BubbyRoosh/morg`
3. Build and install morg `cd morg && make && make install`

## Usage

Simply run morg with the paths to all the files you want sorted:
```
morg path/to/file
morg *.flac
```

Or...

Scan all files recursively with `find` (takes stdin input separated by '\n'):
```
find -name "*.flac" | morg
```

`morg` can also be used to help with searching for music.

`morg -g 'tagfmt' [file ...]` will print each file's metadata according to
the format provided to it. Note the single quotes are needed to prevent '$'
from being escaped for shell variables in most posix shells.

For example, here is a method to get all songs made after the year 1981:
```
find . -name "*.flac" | morg -g '$year$ - $title$' | awk '$1 > 1981'
```

Or to get all unique artists in your library:
```
find . -name "*.flac" | morg --g '$artist$' | sort | uniq
```

Information on command-line arguments can be displayed by passing "-h"
