.PHONY: morg install uninstall clean
default: morg ;
CFLAGS = -Wall -Wextra -pedantic -std=c99 -O3
INCLUDES = -Ilib
LIBS = -ltag_c
BINDIR = /usr/local/bin

morg: src/morg.c
	$(CC) -o $@ $^ $(CFLAGS) $(LIBS) $(INCLUDES)

install: morg
	install -m 755 morg $(DESTDIR)$(BINDIR)/morg

uninstall:
	rm -f $(DESTDIR)$(BINDIR)/morg

clean:
	rm -f morg
