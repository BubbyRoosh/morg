#include <err.h>
#include <errno.h>
#include <limits.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/stat.h>
#include <unistd.h>

#include <taglib/tag_c.h>

#include "argp.h"
#include "helpers.h"

#include "config.h"

int copy         = 0;
int printonly    = 0;
int quiet        = 0;
char *get        = NULL;

void
usage()
{
    printf("Usage: morg [file ...]\n\
\n\
-d, -f, and -g will all have the following sequences replaced when formatting:\n\
    $artist$, $title$, $album$, $track$, $year$\n\
\n\
Arguments:\n\
  -c\n\
      copy files instead of moving them\n\
  -d string (\"$artist$ - $album$\")\n\
      how directories will be named\n\
  -f string (\"$track$ - $title$\")\n\
      how files will be named\n\
  -g string (\"\")\n\
      gets the specified metadata from each file using the same format as\n\
      fileformat and dirformat. Doesn\'t execute anything\n\
  -o string (\"$HOME/Music\")\n\
      where the sorted files/dirs will go\n\
  -p, -p\n\
      only print the altered paths. Don\'t execute anything\n\
  -q\n\
      don\'t print \"/old/path -> /new/path\" for each file\n\
");
    exit(1);
}


char *
reassign(char *in, const char *find, const char *rep)
{
    char *ent = strrep(in, find, rep);
    free(in);
    return ent;
}

char *
replace_tags(TagLib_Tag *t, char *in)
{
    in = reassign(in, "$artist$", taglib_tag_artist(t));
    if (!in) return NULL;
    in = reassign(in, "$album$", taglib_tag_album(t));
    if (!in) return NULL;

    int trk = taglib_tag_track(t);
    size_t trklen = snprintf(NULL, 0, "%d", trk)+1;
    char track[trklen];
    snprintf(track, trklen, "%d", trk);
    in = reassign(in, "$track$", track);
    if (!in) return NULL;

    int yr = taglib_tag_year(t);
    size_t yearlen = snprintf(NULL, 0, "%d", yr)+1;
    char year[yearlen];
    snprintf(year, yearlen, "%d", yr);
    in = reassign(in, "$year$", year);
    if (!in) return NULL;

    in = reassign(in, "$title$", taglib_tag_title(t));
    if (!in) return NULL;

    return in;
}

void *
scan_file(char *filename)
{
    TagLib_File *f = NULL;
    char *newfile  = NULL;

    f = taglib_file_new(filename);
    if (!f) goto defer;

    TagLib_Tag *t = taglib_file_tag(f);
    if (!t) goto defer;

    char *tmp = NULL;
    newfile = strrep(outpath, "$HOME", getenv("HOME"));
    if (!newfile) goto defer;
    size_t nflen = strlen(newfile);
    size_t dflen = strlen(dirformat);
    size_t fflen = strlen(fileformat);


    if (get) {
        char *getstr = calloc(strlen(get)+1, sizeof(char));
        if (!getstr) goto defer;
        memcpy(getstr, get, strlen(get)+1);
        char *tmp = replace_tags(t, getstr);
        if (!tmp) {
            free(getstr);
            goto defer;
        }
        getstr = tmp;
        printf("%s\n", getstr);
        free(getstr);
        goto defer;
    }

    tmp = realloc(
        newfile,
        /*
         * nflen    +  1  + dflen     +  1  + fflen       +   1
         * rootpath + '/' + dirformat + '/' + fileformat  + '\0'
        */
        (nflen + dflen + fflen + 3)*sizeof(char)
    );
    if (!tmp) goto defer;
    newfile = tmp;

    /* Zero new section */
    memset(newfile+nflen, 0, (dflen + fflen + 3)*sizeof(char));

    /* Set the '/'s in the new path */
    newfile[nflen] = '/';
    newfile[nflen+dflen+1] = '/';

    /* Copy over the dirformat and fileformat from config.h */
    memcpy(newfile+nflen+1, dirformat, dflen);
    memcpy(newfile+nflen+dflen+2, fileformat, fflen);

    newfile = replace_tags(t, newfile);
    if (!newfile) goto defer;


    /* Should try to preserve the original extension :^) */
    char *ext = strchr(filename, '.');
    for (;;) {
        char *next = strchr(filename+(ext-filename)+1, '.');
        if (!next) break;
        ext = next;
    }
    if (ext) {
        nflen = strlen(newfile);
        size_t extlen = strlen(ext);
        tmp = realloc(newfile, (nflen + extlen + 1)*sizeof(char));
        if (!tmp) goto defer;
        newfile = tmp;
        memcpy(newfile+nflen, ext, extlen);
        newfile[nflen + extlen] = '\0';
    }

    if (!quiet)
        printf("%s -> %s\n", filename, newfile);

    if (!printonly) {
        if (mkdirall(newfile) == -1) {
            perror("mkdirall");
            goto defer;
        }
        if (link(filename, newfile) != 0) {
            perror("link");
            goto defer;
        }
        if (!copy)
            unlink(filename);
    }

defer:
    if (newfile) free(newfile);
    if (f) taglib_file_free(f);
    return NULL;
}

int
main(int argc, char **argv)
{

    int alloced_files = 0;

    char **files = NULL;
    size_t fileslen = 0;

    ARGBEGIN {
        case 'c': copy++; break;
        case 'p': printonly++; break;
        case 'q': quiet++; break;
        case 'd': dirformat = ARGNEXT(usage()); break;
        case 'f': fileformat = ARGNEXT(usage()); break;
        case 'o': outpath = ARGNEXT(usage()); break;
        case 'g': get = ARGNEXT(usage()); break;
        case '-':
            argc--; argv++;
            goto done_args;
        default: usage();
    } ARGEND;
done_args:

    if (argc) {
        files = argv;
        fileslen = argc;
    } else {
        char *line = calloc(PATH_MAX, sizeof(char));

        for (;fgets(line, PATH_MAX, stdin);) {
            char **tmp = NULL;
            fileslen++;
            line[strlen(line)-1] = '\0';
            tmp = realloc(files, fileslen*sizeof(char*));
            if (!tmp) break;
            files = tmp;
            files[fileslen-1] = line;
            line = calloc(PATH_MAX, sizeof(char));
            if (!line) break;
        }
        alloced_files = 1;
    }

    for (size_t i = 0; i < fileslen; i++)
        scan_file(files[i]);

    if (alloced_files) {
        for (size_t i = 0; i < fileslen; i++)
            free(files[i]);
        free(files);
    }

    return 0;
}
